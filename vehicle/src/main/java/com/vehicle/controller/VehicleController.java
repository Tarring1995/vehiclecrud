package com.vehicle.controller;

import com.vehicle.dao.VehicleDao;
import com.vehicle.domain.Vehicle;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.web.bind.annotation.*;

import java.util.*;
@CrossOrigin(origins = {"http://localhost:8080/vehicles","null"})
@RestController
public class VehicleController {

    private VehicleDao dao = new VehicleDao();
    //@CrossOrigin(origins = {"http://localhost:8080/vehicles","null"})
    @RequestMapping(value = "/vehicles", method = RequestMethod.GET)
    public List<Vehicle> getVehicles() {
        return dao.readVehicle();
    }
    //@CrossOrigin(origins = {"http://localhost:8080/vehicles","null"})
    @RequestMapping(value = "/vehicles/{id}", method = RequestMethod.GET)
    public Vehicle getVehicleById(@PathVariable int id) {
        return dao.readVehicleById(id);
    }

    @ApiImplicitParam(name = "vehicle", value = "Vehicle Object", required = true, dataType = "Vehicle")
    @RequestMapping(value = "/vehicles", method = RequestMethod.POST)
    public String postVehicle(@ModelAttribute Vehicle vehicle) {
        boolean result = dao.createVehicle(vehicle);

        if (result)
            return "success";
        else
            return "false";
    }
    
    @ApiImplicitParam(name = "vehicle", value = "Vehicle Object", required = true, dataType = "Vehicle")
    @RequestMapping(value = "/vehicles", method = RequestMethod.PUT)
    public String putVehicle(@ModelAttribute Vehicle vehicle) {
        boolean result = dao.updateVehicle(vehicle);

        if (result)
            return "success";
        else
            return "false";
    }

    @RequestMapping(value = "/vehicles/{id}", method = RequestMethod.DELETE)
    public String deleteVehicle(@PathVariable int id) {
        boolean result = dao.deleteVehicle(id);

        if (result)
            return "success";
        else
            return "false";
    }

}
