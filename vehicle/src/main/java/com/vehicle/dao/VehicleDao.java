package com.vehicle.dao;

import com.vehicle.domain.Vehicle;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class VehicleDao {

    // JDBC Driver name and Database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/vehicle";

    // user name and password
    static final String USER = "root";
    static final String PASS = "1020";

    // JDBC Connection
    Connection conn = null;

    public VehicleDao() {
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeConn() {
        try {
            this.conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // CREATE
    public boolean createVehicle(Vehicle vehicle) {
        String sql = "INSERT INTO vehicle(id, year, make, model) VALUES(" + vehicle.getId() + "," + vehicle.getYear() +
                ","  + "\"" + vehicle.getMake() + "\"," + "\"" + vehicle.getModel() + "\")";

        int result = 0;
        try {
            PreparedStatement preStmt = conn.prepareStatement(sql);
            result = preStmt.executeUpdate();
            preStmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result == 1;
    }

    // READ
    public List<Vehicle> readVehicle() {
        String sql = "select * from vehicle";

        List<Vehicle> list = new ArrayList<>();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                int id = rs.getInt("id");
                int year = rs.getInt("year");
                String make = rs.getString("make");
                String model = rs.getString("model");

                Vehicle v = new Vehicle(id, year, make, model);
                list.add(v);
            }
            stmt.close();
        } catch(Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    // READ BY ID
    public Vehicle readVehicleById(int requestId) {
        String sql = "select * from vehicle where id=" + requestId;

        Vehicle result = null;
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                int id = rs.getInt("id");
                int year = rs.getInt("year");
                String make = rs.getString("make");
                String model = rs.getString("model");

                result = new Vehicle(id, year, make, model);

            }
            stmt.close();
        } catch(Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    // UPDATE
    public boolean updateVehicle(Vehicle vehicle) {
        String sql1 = "select * from vehicle";

        // check the vehicle whether exists
        boolean isExist = false;
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql1);
            while (rs.next()) {
                int id = rs.getInt("id");
                if (id == vehicle.getId()) {
                    isExist = true;
                }
            }
            stmt.close();
        } catch(Exception e) {
            e.printStackTrace();
        }

        // if exist, delete this vehicle first
        if (isExist) {
            String sql2 = "delete from vehicle where id=" + vehicle.getId();

            try {
                PreparedStatement prestmt = conn.prepareStatement(sql2);
                prestmt.executeUpdate();
                prestmt.close();
            } catch(Exception e) {
                e.printStackTrace();
            }
        }

        // add this vehicle
        String sql = "INSERT INTO vehicle(id, year, make, model) VALUES(" + vehicle.getId() + "," + vehicle.getYear() +
                ","  + "\"" + vehicle.getMake() + "\"," + "\"" + vehicle.getModel() + "\")";

        int result = 0;
        try {
            PreparedStatement preStmt2 = conn.prepareStatement(sql);
            result = preStmt2.executeUpdate();
            preStmt2.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result == 1;
    }

    // DELETE
    public boolean deleteVehicle(int requestId) {
        String sql = "delete from vehicle where id=" + requestId;

        int result = 0;
        try {
            PreparedStatement prestmt = conn.prepareStatement(sql);
            result = prestmt.executeUpdate();
            prestmt.close();
        } catch(Exception e) {
            e.printStackTrace();
        }

        return result == 1;
    }
}
