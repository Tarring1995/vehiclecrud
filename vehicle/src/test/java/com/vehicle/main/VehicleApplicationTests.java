package com.vehicle.main;

import com.vehicle.controller.VehicleController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MockServletContext.class)
@WebAppConfiguration
public class VehicleApplicationTests {

	private MockMvc mvc;

	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.standaloneSetup(new VehicleController()).build();
	}

	@Test
	public void testUserController() throws Exception {
		RequestBuilder request = null;

		// 1. GET vehicles Test
		request = get("/vehicles");
		mvc.perform(request)
				.andExpect(status().isOk());

		// 2. POST vehicles Test
		request = post("/vehicles")
				.param("id", "5")
				.param("year", "2019")
				.param("make", "Toyota")
		        .param("model", "Supra Xfinity");
		mvc.perform(request)
				.andExpect(status().isOk());

		// 3. PUT vehicles Test
		request = put("/vehicles")
				.param("id", "5")
				.param("year", "2020")
				.param("make", "Toyota")
				.param("model", "GR Supra");
		mvc.perform(request)
				.andExpect(status().isOk());

		// 4. GET vehicles/{id} Test
		request = get("/vehicles/1");
		mvc.perform(request)
				.andExpect(status().isOk());

		// 5. DELETE vehicles/{id} Test
		request = delete("/vehicles/1");
		mvc.perform(request)
				.andExpect(status().isOk());
	}

}
